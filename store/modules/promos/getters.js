export default {
    headers(state) {
      return state.headers;
    },
    goods_headers(state) {
      return state.goods_headers;
    },
    gifts_headers(state) {
      return state.gifts_headers;
    },
    products_headers(state) {
      return state.products_headers;
    },
    categories_headers(state) {
      return state.categories_headers;
    },
    statuses(state) {
      return state.statuses;
    },
    stations(state) {
      return state.stations;
    },
    categoriesList(state) {
      return state.categoriesList;
    },
    goodsList(state) {
      return state.goodsList;
    },
    unitsList(state) {
      return state.unitsList;
    },
    volumeUnitsList(state) {
      return state.volumeUnitsList;
    },
    currentPage(state) {
      return state.currentPage;
    },
    stores(state) {
      return state.stores;
    }
  };
  