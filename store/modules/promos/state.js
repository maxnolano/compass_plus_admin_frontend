export default () => ({
headers: [
  { text: 'id', value: 'id' },
  { text: 'st_store_id', value: 'st_store_id' },
  { text: 'st_store_name', value: 'st_store_name' },
  { text: 'title', value: 'title' },
  { text: 'description', value: 'description' },
  { text: 'expire_date', value: 'expire_date' },
  { text: 'status', value: 'status' },
  { text: "", value: "controls", sortable: false },
],
goods_headers: [
  { text: 'Id товара', value: 'id' },
  { text: 'Id акции', value: 'promo_id' },
  // { text: 'Категория товара', value: 'category_id' },
  { text: 'Категория товара', value: 'ct_title' },
  // { text: 'Товар по категории', value: 'item_code' },
  { text: 'Товар по категории', value: 'pr_title' },
  { text: 'Сумма покупки', value: 'amount_to_order' },
  { text: 'Юнит', value: 'amount_unit' },
  { text: 'Дата создания', value: 'created_at' },
  { text: 'Дата изменения', value: 'updated_at' },
  { text: "", value: "controls", sortable: false },
],
gifts_headers: [
  { text: 'Id товара', value: 'id' },
  { text: 'Id акции', value: 'promo_id' },
  { text: 'Тип подарка', value: 'gift_type' },
  // { text: 'Категория товара', value: 'product_category' },
  { text: 'Категория товара', value: 'ct_title' },
  // { text: 'Товар по категории', value: 'product_id' },
  { text: 'Товар по категории', value: 'pr_title' },
  { text: 'Сумма покупки', value: 'amount' },
  { text: 'Юнит', value: 'amount_unit' },
  { text: 'Срок действия', value: 'expire_date' },
  { text: 'Дата создания', value: 'created_at' },
  { text: 'Дата изменения', value: 'updated_at' },
  { text: 'Количество дней', value: 'expire_days' },
  { text: 'Максимальный объём', value: 'max_value' },
  { text: 'Юнит (объёма)', value: 'max_value_unit' },
  { text: "", value: "controls", sortable: false },
],
products_headers: [
  { text: 'Id товара', value: 'id' },
  { text: 'Название', value: 'title_p' },
  { text: 'Код', value: 'item_code' },
  { text: 'Категория', value: 'category_id' },
  { text: 'Описание', value: 'description' },
  { text: 'Цена', value: 'price' },
  { text: 'Количество', value: 'quantity' },
  { text: 'Вес', value: 'weight' },
  { text: 'Юнит', value: 'weight_unit' },
  { text: 'Штрих код', value: 'ean13' },
  { text: 'Картина товара', value: 'img' },
  { text: "", value: "controls", sortable: false },
],
categories_headers: [
  { text: 'Id категории', value: 'id_c' },
  { text: 'Описание', value: 'title_c' },
  { text: 'Id магазина', value: 'store_id_c' },
  { text: 'Вид отдела продажи', value: 'object_type_c' },
  { text: "", value: "controls", sortable: false },
],
  statuses: [
    {text: 'Активный', val: 1},
    {text: 'Не активный', val: 0},
  ],
  stations: ['Compass', 'Royal Petrol', 'Gas Energy'],
  categoriesList: ['Categories1', 'Categories2', 'Categories3'],
  goodsList: ['Goods1', 'Goods2', 'Goods3'],
  unitsList: [
    {
      'text': 'Штук(а)',
      'val': '1'
    },
    {
      'text': 'Тенге',
      'val': '2'
    },
    {
      'text': 'Литр(ов)',
      'val': '3'
    },
    {
      'text': 'Процент(ов)',
      'val': '4'
    }
  ],
  volumeUnitsList: [
    {
      'text': 'Штук(а)',
      'val': '1'
    },
    {
      'text': 'Тенге',
      'val': '2'
    },
    {
      'text': 'Литр(ов)',
      'val': '3'
    }
  ],
  currentPage: '',
  stores: []
  });
  