export default {
    setHeadersAction({ commit }, val) {
      commit("setHeaders", val);
    },
    setStationsAction({ commit }, val) {
      commit("setStations", val);
    },
    setCategoriesListAction({ commit }, val) {
      commit("setCategoriesList", val);
    },
    setGoodsListAction({ commit }, val) {
      commit("setGoodsList", val);
    },
    setUnitsListAction({ commit }, val) {
      commit("setUnitsList", val);
    },
    setVolumeUnitsListAction({ commit }, val) {
      commit("setVolumeUnitsList", val);
    },
    setCurrentPageAction({ commit }, val) {
      commit("setCurrentPage", val);
    },
    setStoresAction({ commit }, val) {
      commit("setStores", val);
    },
  };
  