export default () => ({
headers_promo_history: [
  { text: 'id', value: 'id' },
  { text: 'username', value: 'username' },
  { text: 'promo_id', value: 'promo_id' },
  { text: 'promo_title', value: 'promo_title' },
  { text: 'created_at', value: 'created_at' },
  { text: 'ip_address', value: 'ip_address' },
  { text: "", value: "controls", sortable: false },
],
});
  